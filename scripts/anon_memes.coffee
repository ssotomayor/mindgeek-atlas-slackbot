# Description:
#   Memes from http://memecaptain.com/
#
# Dependencies:
#   None
#
# Commands:
#   <someone> says <text> - Generates a personal meme <text> (use a comma to separate top and bottom captions)
#
# Author:
#   tehsoto

module.exports = (robot) ->

  addPersonalMeme  = (name, imageId) ->
    robot.hear new RegExp('^' + name + ' (?:tells you|says) ([^\,]*)(\,[\s]*(.*))?$', 'i'), (msg) ->
      memeGenerator msg, imageId, msg.match[1], msg.match[3] || ''

  # upload these images at http://memecaptain.com/my

  addPersonalMeme 'gravelg', 'slEe-g'
  addPersonalMeme 'smccready', 'BLKqNQ'
  addPersonalMeme 'tcfaria', 'JpP0PA'
  addPersonalMeme 'sh_murphy', 'IZ16Pg'
  addPersonalMeme 'k_parent', 'F2i2Qg'
  addPersonalMeme 'mayor1', 'PkL5Jw'
  addPersonalMeme 'mayor2', '7X4dkg'
  addPersonalMeme 'happygravelg', 'PbD09Q'
  addPersonalMeme 'msimard', 'bDQqDg'
  addPersonalMeme 'trudeau', 'P1D8nQ'
  addPersonalMeme 'horsegab', 'erhqAQ'

createPostData = (imageID, lowerText, upperText) ->
  data = {
    src_image_id: imageID,
    private: true,
    captions_attributes: [
      {
        text: lowerText.trim(),
        top_left_x_pct: 0.05,
        top_left_y_pct: 0.75,
        width_pct: 0.9,
        height_pct: 0.25
      },
      {
        text: upperText.trim(),
        top_left_x_pct: 0.05,
        top_left_y_pct: 0,
        width_pct: 0.9,
        height_pct: 0.25
      }
    ]
  }
  return JSON.stringify(data)


memeGenerator = (msg, imageID, upperText, lowerText) ->
  MEME_CAPTAIN = 'http://memecaptain.com/gend_images'
  baseError = 'Sorry, I couldn\'t generate that meme.'
  reasonError = 'Unexpected status from memecaptian.com:'

  processResult = (err, res, body) ->
    return msg.reply "#{baseError} #{err}" if err
    if res.statusCode == 301
      msg.http(res.headers.location).get() processResult
      return
    if res.statusCode == 202 || res.statusCode == 200 # memecaptain API success
      timer = setInterval(->
        msg.http(res.headers.location).get() (err, res, body) ->
          return msg.reply "#{baseError} #{err}" if err
          if res.statusCode == 303
            msg.send res.headers.location
            clearInterval(timer)
      , 2000)
    if res.statusCode > 300
      msg.reply "#{baseError} #{reasonError} #{res.statusCode}"


  data = createPostData(imageID, lowerText, upperText)
  msg.robot.http(MEME_CAPTAIN)
  .header('accept', 'application/json')
  .header('Content-type', 'application/json')
  .post(data) processResult
