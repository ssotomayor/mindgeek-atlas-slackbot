# Description:
#   Which is Better?
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot which is better[?] <text> or <text>?
#   hubot who is better[?] <text> or <text>?
#   hubot which is worse[?] <text> or <text>?
#   hubot who is worse[?] <text> or <text>?
#
# Author:
#   cpradio

uhh_what = [
  "I could tell you, but then I'd have to kill you",
  "Answering that would be a matter of national security",
  "You can't possibly compare them!",
  "Both hold a special place in my heart"
]

random_res = [
  "That's not nice",
  "So predictable",
  "You hold a special place in my heart.. too bad i don't have one"
]

random_res_humans = [
  "Humans are useless",
  "Not like i care, i'm just a bot",
  "..."
]

_randomNum = (max,min=0) ->
  return Math.floor(Math.random() * (max - min) + min)

module.exports = (robot) ->

  robot.respond /(which|who) is (better|worse)\?* (.*) or (.*?)\??$/i, (msg) ->
    choosen_response = msg.random [1..5]
    if choosen_response >= 3
      msg.send msg.random uhh_what
    else
      msg.send "Clearly #{msg.match[choosen_response + 2]} is #{msg.match[2]}"

  robot.respond /(stfu|Fuck you)/i, (msg) ->
    reply = msg.random random_res
    reply += " #{msg.message.user.name}"
    msg.send reply

  robot.respond /(ping soto)/i, (msg) ->
    reply = msg.random random_res
    reply += " #{msg.message.user.name}"
    robot.send {room: "random"}, "zap!"

  robot.hear /(humans?|humanity|jobs?)/i, (msg) ->
    if _randomNum(10, 0) >= 4
      msg.send msg.random random_res_humans